<?php
/*
 * This file is part of the Yii2 GeoNames extension.
 *
 * (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace diggindata\geonames\controllers;

use diggindata\geonames\models\CitySearchForm;

use Yii;
use yii\web\Controller;

use diggindata\geonames\models\Geoname;

/**
 * Default controller for the `geonames` module
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * Shows an overview of CRUF pages for the various tables
     *
     * @return string
     */
    public function actionSearchCity()
    {
        $model = new CitySearchForm();
        $debug = null;
        if ($model->load(Yii::$app->request->post())) {
            ob_start(); 
            if($result = $model->search()) {
                Yii::$app->session->setFlash('citySearchFormSubmitted');
                echo $this->vd($result);
                //return $this->refresh();
            }
            $debug = ob_get_clean();
        }
        return $this->render('search-city', [
            'model' => $model,
            'debug' => $debug,
        ]);;
    }

    /**
     * Shows a form with an auto-complete input and a Google Map
     *
     * @return string
     */
    public function actionAutoComplete()
    {
        return $this->render('auto-complete');
    }

    /**
     * Renders a JSON array with city search results
     *
     *  Example:
     *  [
     *      {"id":2867543,"label":"Münster","value":"Münster"},
     *      ...
     *  ]
     *
     * @return mixed
     */
    public function actionCityAutocomplete($term, $country=null)
    {
        $result = array();
        $resultTmp = array();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $records = Geoname::find()->admin1Name()->admin2Name()->city($term);
        if(!empty($country))
            $records->country($country);
        $records = $records ->all(); 
        foreach($records as $record) {
            $resultTmp[join('|', array($record->name, $record->featureClass, $record->featureCode))] = $record;
        }
        foreach($resultTmp as $key=>$record) {
            list($name, $fclass, $fcode) = explode('|', $key);
            if($fclass=='P' and ($fcode=='PPL' or $fcode=='PPLA4')) {
                if(array_key_exists($name.'|A|ADM4', $resultTmp))
                    unset($resultTmp[$name.'|A|ADM4']);
            }
        }

        foreach($resultTmp as $record) {
            $item = new \StdClass;
            $item->id = $record->geonameId;
            $item->label = $record->name;
            $item->value = $record->name;
            $result[] = $item;
        }
        return $result; 
    }

    /**
     * Renders a div with vVarDumper output of an object or an array
     */
    public function vd($data)
    {
        return '<div class="well">'.\yii\helpers\VarDumper::dumpAsString($data, 10, true).'</div>';

    }
    
}
