<?php

namespace diggindata\geonames;

use Yii;

/**
 * geonames module definition class
 */
class Module extends \yii\base\Module
{
    const VERSION = '0.0.4';

    /** @var array Model map */
    public $modelMap = [];

    /**
     * @var string The prefix for geonames module URL.
     *
     * @See [[GroupUrlRule::prefix]]
     */
    public $urlPrefix = 'geonames';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'diggindata\geonames\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        // echo "Module geonames init()\n";
        parent::init();

        // custom initialization code goes here
        if (Yii::$app instanceof \yii\console\Application) {
            // echo "Set controllerNamespace\n";
            $this->controllerNamespace = 'app\modules\geonames\commands';
        }

    }
}
