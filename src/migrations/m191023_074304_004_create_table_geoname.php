<?php

use yii\db\Migration;

class m191023_074304_004_create_table_geoname extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%geoname}}', [
            'geonameId' => $this->primaryKey()->unsigned(),
            'name' => $this->string(200),
            'asciiName' => $this->string(200),
            'alternateNames' => $this->string(4000),
            'latitude' => $this->decimal(10, 7),
            'longitude' => $this->decimal(10, 7),
            'featureClass' => $this->char(),
            'featureCode' => $this->string(10),
            'countryCode' => $this->string(2),
            'cc2' => $this->string(200),
            'admin1Code' => $this->string(20),
            'admin2Code' => $this->string(80),
            'admin3Code' => $this->string(20),
            'admin4Code' => $this->string(20),
            'population' => $this->integer(),
            'elevation' => $this->integer(),
            'dem' => $this->integer(),
            'timezoneId' => $this->string(40),
            'modificationDate' => $this->date(),
        ], $tableOptions);

        $this->createIndex('fclass', '{{%geoname}}', 'featureClass');
        $this->createIndex('longitude', '{{%geoname}}', 'longitude');
        $this->createIndex('latitude', '{{%geoname}}', 'latitude');
        $this->createIndex('asciiname', '{{%geoname}}', 'asciiName');
        $this->createIndex('timezone', '{{%geoname}}', 'timezoneId');
        $this->createIndex('name', '{{%geoname}}', 'name');
        $this->createIndex('elevation', '{{%geoname}}', 'elevation');
        $this->createIndex('population', '{{%geoname}}', 'population');
        $this->createIndex('admin1', '{{%geoname}}', 'admin1Code');
        $this->createIndex('cc2', '{{%geoname}}', 'cc2');
        $this->createIndex('country', '{{%geoname}}', 'countryCode');
        $this->createIndex('fcode', '{{%geoname}}', 'featureCode');
    }

    public function down()
    {
        $this->dropTable('{{%geoname}}');
    }
}
