<?php

use yii\db\Migration;

class m191023_074304_009_create_table_admin1codeascii extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admin1codeascii}}', [
            'code' => $this->char(15)->notNull(),
            'name' => $this->text(),
            'nameAscii' => $this->text(),
            'geonameId' => $this->primaryKey()->unsigned(),
        ], $tableOptions);

        $this->createIndex('geonameid', '{{%admin1codeascii}}', 'geonameId');
        $this->createIndex('nameAscii', '{{%admin1codeascii}}', 'nameAscii(20)');
        $this->createIndex('name', '{{%admin1codeascii}}', 'name(20)');
        $this->addForeignKey('admin1codeascii_ibfk_1', '{{%admin1codeascii}}', 'geonameId', '{{%geoname}}', 'geonameId', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%admin1codeascii}}');
    }
}
