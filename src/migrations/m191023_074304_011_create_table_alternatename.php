<?php

use yii\db\Migration;

class m191023_074304_011_create_table_alternatename extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%alternatename}}', [
            'alternatenameId' => $this->primaryKey(),
            'geonameId' => $this->integer()->unsigned(),
            'isoLanguage' => $this->string(7),
            'alternateName' => $this->string(200),
            'isPreferredName' => $this->tinyInteger(1),
            'isShortName' => $this->tinyInteger(1),
            'isColloquial' => $this->tinyInteger(1),
            'isHistoric' => $this->tinyInteger(1),
        ], $tableOptions);

        $this->createIndex('alternateName', '{{%alternatename}}', 'alternateName');
        $this->createIndex('isoLanguage', '{{%alternatename}}', 'isoLanguage');
        $this->createIndex('geonameid', '{{%alternatename}}', 'geonameId');
        $this->addForeignKey('alternatename_ibfk_1', '{{%alternatename}}', 'geonameId', '{{%geoname}}', 'geonameId', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%alternatename}}');
    }
}
