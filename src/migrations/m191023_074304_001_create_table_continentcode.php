<?php

use yii\db\Migration;

class m191023_074304_001_create_table_continentcode extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%continentcode}}', [
            'code' => $this->char(2),
            'name' => $this->string(20),
            'geonameId' => $this->integer()->unsigned(),
        ], $tableOptions);

        $this->createIndex('geonameid', '{{%continentcode}}', 'geonameId');
        $this->createIndex('name', '{{%continentcode}}', 'name');
        $this->createIndex('code', '{{%continentcode}}', 'code');
    }

    public function down()
    {
        $this->dropTable('{{%continentcode}}');
    }
}
