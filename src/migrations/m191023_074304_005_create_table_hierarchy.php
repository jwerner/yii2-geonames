<?php

use yii\db\Migration;

class m191023_074304_005_create_table_hierarchy extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%hierarchy}}', [
            'parentId' => $this->integer(),
            'childId' => $this->integer(),
            'type' => $this->string(50),
        ], $tableOptions);

        $this->createIndex('childId', '{{%hierarchy}}', 'childId');
        $this->createIndex('parentId', '{{%hierarchy}}', 'parentId');
    }

    public function down()
    {
        $this->dropTable('{{%hierarchy}}');
    }
}
