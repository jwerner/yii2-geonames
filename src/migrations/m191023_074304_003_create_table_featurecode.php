<?php

use yii\db\Migration;

class m191023_074304_003_create_table_featurecode extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%featurecode}}', [
            'code' => $this->char(7)->notNull()->append('PRIMARY KEY'),
            'name' => $this->string(200),
            'description' => $this->text(),
        ], $tableOptions);

        $this->createIndex('name', '{{%featurecode}}', 'name');
    }

    public function down()
    {
        $this->dropTable('{{%featurecode}}');
    }
}
