<?php

use yii\db\Migration;

class m191023_074304_002_create_table_countryinfo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%countryinfo}}', [
            'isoAlpha2' => $this->char(2),
            'isoAlpha3' => $this->char(3),
            'isoNumeric' => $this->integer(),
            'fipsCode' => $this->string(3),
            'name' => $this->string(200),
            'capital' => $this->string(200),
            'areaInSqKm' => $this->double(),
            'population' => $this->integer(),
            'continent' => $this->char(2),
            'continentId' => $this->integer(10)->unsigned(),
            'tld' => $this->char(3),
            'currency' => $this->char(3),
            'currencyName' => $this->char(20),
            'phone' => $this->char(10),
            'postalCodeFormat' => $this->string(100),
            'postalCodeRegex' => $this->string(),
            'languages' => $this->string(200),
            'geonameId' => $this->primaryKey(),
            'neighbours' => $this->char(100),
            'equivalentFipsCode' => $this->char(10),
        ], $tableOptions);

        $this->createIndex('name', '{{%countryinfo}}', 'name');
        $this->createIndex('fips_code', '{{%countryinfo}}', 'fipsCode');
        $this->createIndex('iso_numeric', '{{%countryinfo}}', 'isoNumeric');
        $this->createIndex('iso_alpha3', '{{%countryinfo}}', 'isoAlpha3');
        $this->createIndex('iso_alpha2', '{{%countryinfo}}', 'isoAlpha2');
    }

    public function down()
    {
        $this->dropTable('{{%countryinfo}}');
    }
}
