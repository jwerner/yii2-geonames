<?php

use yii\db\Migration;

class m191023_074304_010_create_table_admin2code extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admin2code}}', [
            'code' => $this->char(15),
            'name' => $this->text(),
            'nameAscii' => $this->text(),
            'geonameId' => $this->primaryKey()->unsigned(),
        ], $tableOptions);

        $this->createIndex('nameAscii', '{{%admin2code}}', 'nameAscii(80)');
        $this->createIndex('name', '{{%admin2code}}', 'name(80)');
        $this->createIndex('code', '{{%admin2code}}', 'code');
        $this->addForeignKey('admin2code_ibfk_1', '{{%admin2code}}', 'geonameId', '{{%geoname}}', 'geonameId', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%admin2code}}');
    }
}
