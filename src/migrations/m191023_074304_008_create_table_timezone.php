<?php

use yii\db\Migration;

class m191023_074304_008_create_table_timezone extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%timezone}}', [
            'timeZoneId' => $this->string(40)->notNull()->append('PRIMARY KEY'),
            'countryCode' => $this->char(2),
            'gmtOffset' => $this->decimal(2, 1),
            'dstOffset' => $this->decimal(2, 1),
            'rawOffset' => $this->decimal(2, 1),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%timezone}}');
    }
}
