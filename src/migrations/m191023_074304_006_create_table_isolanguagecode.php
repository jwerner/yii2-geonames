<?php

use yii\db\Migration;

class m191023_074304_006_create_table_isolanguagecode extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%isolanguagecode}}', [
            'iso_639_3' => $this->char(4)->notNull()->append('PRIMARY KEY'),
            'iso_639_2' => $this->string(50),
            'iso_639_1' => $this->string(50),
            'languageName' => $this->string(200),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%isolanguagecode}}');
    }
}
