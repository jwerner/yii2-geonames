<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace diggindata\geonames\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\console\ExitCode;

/**
 * This command manages downloading and seeding of GeonNames data
 *
 * @author Evren Yurtesen
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class GeonamesController extends Controller
{
    // download
    public $update = false;

    // seed
    public $refresh = false;
    public $table;

    /**
     * Buffer size to use when executing SQL statements in number of rows
     * I experimented and 1000 is a very reasonable value. Larvger values
     * (5000+) may cause too many placeholders error in eloquent.
     *
     * @var int
     */
    protected $bufferSize = 10;

    /**
     * Postfix for postal code $files key
     */
    protected $postalCodePostfix = '_Zip';

    /**
     * This is used to map continent keys with geoname_id values
     *
     * @var array
     */
    protected $continentCodes = array(
        'AF' => 6255146,
        'AS' => 6255147,
        'EU' => 6255148,
        'NA' => 6255149,
        'OC' => 6255151,
        'SA' => 6255150,
        'AN' => 6255152,
    );

    /**
     * This is the main list of url,filename and table values we will work with
     *
     * @var array
     */
    protected $files = array( // {{{ 
        'timeZones' => array(
            'url' => 'http://download.geonames.org/export/dump/timeZones.txt',
            'filename' => 'timeZones',
            'table' => 'timezone',
        ),
        'allCountries' => array(
            'url' => 'http://download.geonames.org/export/dump/allCountries.zip',
            'filename' => 'allCountries',
            'table' => 'geoname'
        ),
        'allCountriesPostCodes' => array(
            'url' => 'http://download.geonames.org/export/zip/allCountries.zip',
            'filename' => 'allCountries',
            'table' => 'postalCode'
        ),
        'countryInfo' => array(
            'url' => 'http://download.geonames.org/export/dump/countryInfo.txt',
            'filename' => 'countryInfo',
            'table' => 'countryinfo'
        ),
        'iso-languagecodes' => array(
            'url' => 'http://download.geonames.org/export/dump/iso-languagecodes.txt',
            'filename' => 'iso-languagecodes',
            'table' => 'isolanguagecode',
        ),
        'alternateNames' => array(
            'url' => 'http://download.geonames.org/export/dump/alternateNames.zip',
            'filename' => 'alternateNames',
            'table' => 'alternatename',
        ),
        'hierarchy' => array(
            'url' => 'http://download.geonames.org/export/dump/hierarchy.zip',
            'filename' => 'hierarchy',
            'table' => 'hierarchy',
        ),
        'admin1CodesASCII' => array(
            'url' => 'http://download.geonames.org/export/dump/admin1CodesASCII.txt',
            'filename' => 'admin1CodesASCII',
            'table' => 'admin1codeascii',
        ),
        'admin2Codes' => array(
            'url' => 'http://download.geonames.org/export/dump/admin2Codes.txt',
            'filename' => 'admin2Codes',
            'table' => 'admin2code',
        ),
        // Todo: figure out how to manage different languages
        'featureCodes' => array(
            'url' => 'http://download.geonames.org/export/dump/featureCodes_en.txt',
            'filename' => 'featureCodes_en',
            'table' => 'featurecode',
        )
        // }}} 
    );

    /**
     * List of tables which are already truncated for avoiding double truncation
     * in case if we are importing multiple files into same table.
     * @var array
     */
    protected $truncatedTables = array();

    private $_timeStart;
    private $_timeEnd;
    
    // {{{ options
    public function options($actionID)
    {
        switch($actionID)   {
        case 'download':
            return ['update'];
            break;
        case 'seed':
            return ['update', 'refresh', 'table'];
            break;
        }
    } // }}} 
    // {{{ optionAliases
    public function optionAliases()
    {
        return [
            'u' => 'update',
            'r' => 'refresh',
            't' => 'table',
        ];
    } // }}} 
    // {{{ actionProgress
    public function actionProgress()
    {
        $max = 10;
        $this->storeTimeStart();
        Console::startProgress(0, $max, 'Counting objects: ', false);
        for ($n = 1; $n <= 10; $n++) {
            usleep(1000000);
            Console::updateProgress($n, $max);
        }
        Console::endProgress("done." . PHP_EOL);
        $time = $this->getTimeDuration();
        echo 'Total Duration: '. Yii::$app->formatter->asDuration($time);
    } // }}} 
    // {{{ actionDownload
    /**
     * Downloads geonames database txt/zip files
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionDownload()
    {
        $this->storeTimeStart();
        if($this->update)
            echo "UPDATE\n";

        $this->downloadAllFiles();
        $time = $this->getTimeDuration();
        $this->line( 'Total Duration: '. Yii::$app->formatter->asDuration($time) );
        return ExitCode::OK;
    } // }}} 
    // {{{ actionSeed
    /**
     * Seed the database from geonames files
     */
    public function actionSeed()
    {
        $this->storeTimeStart();
        /* DEBUG
        echo "update: ".$this->update."\n";
        echo "refresh: ".$this->refresh."\n";
        echo "table: ".$this->table."\n";
        */

        if (isset($this->table)) {
            foreach ($this->files as $name => $file) {
                if ($file['table'] == $this->table) {
                    $this->downloadFile($name, $this->update);
                    $this->parseGeonamesText($name, $this->refresh);
                    return;
                }
            }
            $this->line('<error>Table Not Found: </error> Table '.$table.'not found in configuration');
            return;
        } else {
            $this->downloadAllFiles();
            // Check if we have all the tables
            foreach ($this->getFilesArray() as $name => $file) {
                if (Yii::$app->db->getTableSchema($file['table'])!==false) {
                    $this->parseGeonamesText($name, $this->refresh);
                } else {
                    throw new RuntimeException($file['table'] . ' table not found. Did you run geoname:install then run migrate ?');
                }
            }
        }
        $this->line( 'Finished: Requested actions have been completed!' );
        $time = $this->getTimeDuration();
        $this->line( 'Total Duration: '. Yii::$app->formatter->asDuration($time) );
    } // }}} 
    // {{{ parseGeonamesText
    /**
     * Parses the array created from different geonames file lines
     * and converts into key=>value type array
     *
     * @param string $name The config name of the file
     * @param boolean $refresh Set true for truncating table before inserting rows
     *
     */
    protected function parseGeonamesText($name, $refresh = false)
    {
        $fieldsArray = array(
            'allCountries' => function ($row) { // {{{ 
                return array(
                    'geonameId' => $row[0],
                    'name' => $row[1],
                    'asciiName' => $row[2] ? $row[2] : null,
                    'alternateNames' => $row[3] ? $row[3] : null,
                    'latitude' => $row[4],
                    'longitude' => $row[5],
                    'featureClass' => $row[6] ? $row[6] : null,
                    'featureCode' => $row[7] ? $row[7] : null,
                    'countryCode' => $row[8],
                    'cc2' => $row[9] ? $row[9] : null,
                    'admin1Code' => $row[10] ? $row[10] : null,
                    'admin2Code' => $row[11] ? $row[11] : null,
                    'admin3Code' => $row[12] ? $row[12] : null,
                    'admin4Code' => $row[13] ? $row[13] : null,
                    'population' => $row[14] ? $row[14] : null,
                    'elevation' => $row[15] ? $row[15] : null,
                    'dem' => $row[16] ? $row[16] : null,
                    'timezoneId' => $row[17] ? $row[17] : null,
                    'modificationDate' => trim($row[18]),
                );
            }, // }}} 
            'allCountriesPostCodes' => function ($row) { // {{{ 
                return array(
                    'countryCode'       => $row[0],
                    'postalCode'        => $row[1],
                    'placeName' => $row[2] ? $row[2] : null,
                    'admin1Name' => $row[3] ? $row[3] : null,
                    'admin1Code' => $row[4] ? $row[4] : null,
                    'admin2Name' => $row[5] ? $row[5] : null,
                    'admin2Code' => $row[6] ? $row[6] : null,
                    'admin3Name' => $row[7] ? $row[7] : null,
                    'admin3Code' => $row[8] ? $row[8] : null,
                    'latitude' => $row[9] ? $row[9] : null,
                    'longitude' => $row[10] ? $row[10] : null,
                    'accuracy' => $row[11] ? $row[11] : null,
                );
            }, // }}} 
            'iso-languagecodes' => function ($row) { // {{{ 
                return array(
                    'iso_639_3' => $row[0],
                    'iso_639_2' => $row[1] ? $row[1] : null,
                    'iso_639_1' => $row[2] ? $row[2] : null,
                    'languageName' => trim($row[3]),
                );
            }, // }}} 
            'hierarchy' => function ($row) { // {{{ 
                return array(
                    'parentId' => $row[0],
                    'childId' => $row[1],
                    'type' => trim($row[2]),
                );
            }, // }}} 
            'alternateNames' => function ($row) { // {{{ 
                return array(
                    'alternatenameId' => $row[0],
                    'geonameId' => $row[1],
                    'isoLanguage' => $row[2] ? $row[2] : null,
                    'alternateName' => $row[3] ? $row[3] : null,
                    'isPreferredName' => $row[4] ? 1 : 0,
                    'isShortName' => $row[5] ? 1 : 0,
                    'isColloquial' => $row[6] ? 1 : 0,
                    'isHistoric' => $row[7] ? 1 : 0,
                );
            }, // }}}
            'timeZones' => function ($row) { // {{{ 
                // Skip unusual comment line in this file
                if ($row[0] === 'CountryCode') return null;
                return array(
                    'countryCode' => $row[0],
                    'timeZoneId' => $row[1],
                    'gmtOffset' => $row[2],
                    'dstOffset' => $row[3],
                    'rawOffset' => $row[4]
                );
            }, // }}} 
            'countryInfo' => function ($row) { // {{{ 
                return array(
                    'isoAlpha2' => $row[0],
                    'isoAlpha3' => $row[1],
                    'isoNumeric' => $row[2],
                    'fipsCode' => $row[3] ? $row[3] : null,
                    'name' => $row[4] ? $row[4] : null,
                    'capital' => $row[5] ? $row[5] : null,
                    'areaInSqKm' => $row[6] ? $row[6] : null,
                    'population' => $row[7] ? $row[7] : null,
                    'continent' => $row[8],
                    'continentId' => $this->continentCodes[$row[8]],
                    'tld' => $row[9] ? $row[9] : null,
                    'currency' => $row[10] ? $row[10] : null,
                    'currencyName' => $row[11] ? $row[11] : null,
                    'phone' => $row[12] ? $row[12] : null,
                    'postalCodeFormat' => $row[13] ? $row[13] : null,
                    'postalCodeRegex' => $row[14] ? $row[14] : null,
                    'languages' => $row[15] ? $row[15] : null,
                    'geonameId' => $row[16] ? $row[16] : null,
                    'neighbours' => $row[17] ? $row[17] : null,
                    'equivalentFipsCode' => $row[18] ? trim($row[18]) : null,
                );
            }, // }}} 
            'featureCodes' => function ($row) { // {{{ 
                return array(
                    'code' => $row[0],
                    'name' => $row[1],
                    'description' => trim($row[2]),
                );
            }, // }}} 
            'admin1CodesASCII' => function ($row) { // {{{ 
                return array(
                    'code' => $row[0],
                    'name' => $row[1],
                    'nameAscii' => $row[2],
                    'geonameId' => $row[3],
                );
            }, // }}} 
            'admin2Codes' => function ($row) { // {{{ 
                return array(
                    'code' => $row[0],
                    'name' => $row[1],
                    'nameAscii' => $row[2],
                    'geonameId' => $row[3],
                ); 
            }, // }}} 
        );
        // This will greatly improve the performance of inserts
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=0;')->execute();
        $tableName = $this->files[$name]['table'];
        // If table is empty or we are refreshing, truncate it unless it was recently truncated!
        if (!in_array($tableName,$this->truncatedTables) &&
            (Yii::$app->db->createCommand('SELECt COUNT(*) FROM {{'.$tableName.'}}')->queryScalar() === 0 || $this->refresh)
        ) {
            $this->truncatedTables[] = $tableName;
            $this->line('Database: Truncating table ' . $tableName);
            Yii::$app->db->createCommand('TRUNCATE TABLE {{'.$tableName.'}}')->execute();
        }
        $buffer = array();
        // If it is a custom country code, use allCountries
        if (in_array($name, $this->config('geonames.countries')))
            $fields = $fieldsArray['allCountries'];
        elseif(substr($name, -strlen($this->postalCodePostfix)) == $this->postalCodePostfix) {
            if (in_array(substr($name, 0, -4), $this->config('geonames.countries')))
                $fields = $fieldsArray['allCountriesPostCodes'];
        } else
            $fields = $fieldsArray[$name];
        $this->parseFile($name, function ($row) use (&$buffer, $fields, $tableName) {
            $insert = $fields($row);
            if (isset($insert) && is_array($insert)) {
                $buffer[] = $insert;
            }
            if (count($buffer) === $this->bufferSize) {
                // $this->updateOrInsertMultiple($tableName, $buffer);
                $this->batchInsert($tableName, $buffer);
                $buffer = array();
            }
        });
        // Insert leftovers in buffer
        if (count($buffer) > 0) {
            // $this->updateOrInsertMultiple($tableName, $buffer);
            $this->batchInsert($tableName, $buffer);
        }
        Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS=1;')->execute();
    } // }}} 
    // {{{ parseFile
    /**
     * Parse a given file and insert into databaase using closure.
     *
     * @param  string $name
     * @param  Closure $callback
     * @return void
     */
    protected function parseFile($name, \Closure $callback)
    {
        // $this->line('Parsing File: '.$name);
        $url = $this->files[$name]['url'];
        $basename = basename($url);
        $subPath = '';
        if(substr($name, -strlen($this->postalCodePostfix))==$this->postalCodePostfix)
            $subPath = '/zip';
        $storagePath = $this->config('geonames.storagePath').$subPath;
        // Final file path
        $path = $storagePath . '/' . $this->files[$name]['filename'] . '.txt';
        // Check if file exists (should be since we just downloaded them)
        $downloadedFilePath = $storagePath . '/' . $basename;
        if (!file_exists($downloadedFilePath)) {
            throw new \RuntimeException('File does not exist: ' . $downloadedFilePath);
        }
        // If it is a zip file we must unzip it
        if (substr($basename, -4) === '.zip') {
            $this->unZip($name);
        }
        $steps = $this->getLineCount($path);
        /* @var $output OutputStyle */
        // $output = $this->getOutput();
        //$bar = $output->createProgressBar($steps);
        // $bar->setFormat('<info>Seeding File:</info> ' . basename($path) . ' %current%/%max% %bar% %percent%% <info>Remaining Time:</info> %remaining%');
        // echo '<info>Seeding File:</info> ' . basename($path)."\n";
        Console::startProgress(0, $linesCount = $steps, 'Seeding File: ' . basename($path).' ', false);

        $steps = 0;
        $fh = fopen($path, 'r');
        if (!$fh) {
            throw new RuntimeException("Can not open file: $path");
        }
        while (!feof($fh)) {
            $line = fgets($fh);
            // ignore empty lines and comments
            if (!$line or $line === '' or strpos($line, '#') === 0) continue;
            // Geonames format is tab seperated
            $line = explode("\t", $line);
            // Insert using closure
            $callback($line);
            $steps++;
            // if (isset($bar) && $steps % ($this->bufferSize * 10) === 0) {
            if ($steps % ($this->bufferSize * 50) === 0) {
                //$bar->advance($this->bufferSize * 10);
                Console::updateProgress($steps, $linesCount);
            }
        }
        fclose($fh);
        if (isset($bar)) {
            //$bar->finish();
            // $output->newLine();
        }
        Console::updateProgress($steps, $linesCount);
        Console::endProgress("OK  " . PHP_EOL);
        
        // If we wont keep txt version delete file
        if (substr($basename, -4) === '.zip' && !$this->config('geonames.keepTxt')) {
            $this->line('Removing File: ' . basename($path));
            unlink($path);
        }
    } // }}} 
    // {{{ getLineCount
    /**
     * Read the file and get line count
     * Not very efficient but does the job well...
     *
     * @param  string $path
     * @return int $count
     */
    protected function getLineCount($path)
    {
        $fh = fopen($path, 'r');
        if (!$fh) {
            throw new RuntimeException("Can not open file: $path");
        }
        $fileSize = @filesize($path);
        /* @var $output OutputStyle */
        // $output = $this->getOutput();
        //$bar = $output->createProgressBar($fileSize);
        //$bar->setFormat('<info>Reading File:</info> ' . basename($path) . ' %bar% %percent%% <info>Remaining Time:</info> %remaining%');
        Console::startProgress(0, $fileSize, 'Reading File: ' . basename($path).' ', false);
        $steps = 0;
        $currentSize = 0;
        while (!feof($fh)) {
            $line = fgets($fh);
            $currentSize += strlen($line);
            // ignore empty lines and comments
            if (!$line or $line === '' or strpos($line, '#') === 0) continue;
            $steps++;
            // Reading is so much faster, must slow down advances
            //if (isset($bar) && $steps % ($this->bufferSize * 100) === 0) {
            if ($steps % ($this->bufferSize * 500) === 0) {
                //$bar->advance($currentSize);
                Console::updateProgress($currentSize, $fileSize);
                $currentSize = 0;
            }
        }
        fclose($fh);
        if (isset($bar)) {
            //$bar->finish();
            //$output->newLine();
            echo "\n";
        }
        Console::updateProgress($fileSize, $fileSize);
        Console::endProgress("OK  " . PHP_EOL);
        return $steps;
    } // }}} 
    // {{{ OBS: updateOrInsertMultiple
    /**
     * OBSOLETE: Replaced with batchInsert
     *
     * Create and run a single SQL INSERT query for multiple rows of data.
     * $data argument is an array of database row arrays in key=>value pairs.
     * This speeds up inserts ~50 times compared to line-by-line inserts.
     *
     * Note: The $data must be an array of arrays and have at least 2 elements.
     *
     * @param  string $tableName
     * @param  array (array()) $data
     * @return boolean
     */
    protected function updateOrInsertMultiple($tableName, $data)
    {
        // print_r($data[0]);
        return;
        foreach ($data as $i=>$rows) {
            //Yii::$app->db->createCommand()->upsert('{{tableName}}', $data[$i], $data[$i]);
        }
        //return;
        $fields = '`' . implode('`,`', array_keys($data[0])) . '`';
        // Create strings for variables
        $questionMarks = '';
        $updateList = '';
        foreach (array_keys($data[0]) as $key) {
            $updateList .= '`' . $key . '`' . '=VALUES(' . '`' . $key . '`' . '),';
            $questionMarks .=  '?,';
        }
        // Remove last extra comma
        $updateList = rtrim($updateList, ',');
        $questionMarks = rtrim($questionMarks, ',');
        // If files were not in sync, there may be missing entries in some tables
        $sql = /** @lang text */
            'INSERT IGNORE INTO ' . $tableName . ' (' . $fields . ') ';
        $sql .= 'VALUES ' . PHP_EOL;
        // Set placeholders
        $sql .= '(' . $questionMarks . ')';
        for ($i = 1; $i < count($data); $i++) {
            $sql .= ',' . PHP_EOL . '(' . $questionMarks . ')';
        }
        $sql .= PHP_EOL;
        $valueArray = array();
        foreach ($data as $rows) {
            foreach ($rows as $key => $value) {
                $valueArray[':'.$key] = $value;
            }
        }
        // Set action on duplicate key updates
        $sql .= "ON DUPLICATE KEY UPDATE ";
        $sql .= $updateList;

        echo $sql."\n";
        //return DB::insert($sql, $valueArray);
        return true;
        //return Yii::$app->db->DB::insert($sql, $valueArray);
    } // }}} 
    // {{{ batchInsert
    public function batchInsert($table, $rows)
    {
        $columns = array_keys($rows[0]);
        $updateList = '';
        foreach ($columns as $key) {
            $updateList .= '`' . $key . '`' . '=VALUES(' . '`' . $key . '`' . '),';
        }
        // Remove last extra comma
        $updateList = rtrim($updateList, ',');

        // Is array empty? Nothing to insert!
        if (empty($rows)) {
            return true;
        }
        // Get the column count. Are we inserting all columns or just the specific columns?
        $columnCount = !empty($columns) ? count($columns) : count(reset($rows));
        // Build the column list
        $columnList = !empty($columns) ? '('.implode(', ', $columns).')' : '';
        // Build value placeholders for single row
        $rowPlaceholder = ' ('.implode(', ', array_fill(1, $columnCount, '?')).')';
        // Build the whole prepared query
        $query = sprintf(
            'INSERT IGNORE INTO %s %s VALUES %s ON DUPLICATE KEY UPDATE %s',
            $table,
            $columnList,
            implode(', ', array_fill(1, count($rows), $rowPlaceholder)),
            $updateList
        );
        // echo $query."\n";
        // Prepare PDO statement
        $statement = Yii::$app->db->pdo->prepare($query);
        // Flatten the value array (we are using ? placeholders)
        $data = array();
        foreach ($rows as $rowData) {
            $data = array_merge($data, array_values($rowData));
        }
        // Did the insert go successfully?
        return $statement->execute($data);
    } // }}} 
    // {{{ downloadAllFiles
    /**
     * Download a file if it does not exist
     *
     * @param Boolean $update Update files
     *
     */
    protected function downloadAllFiles()
    {
        $files = array_keys($this->getFilesArray());
        foreach ($files as $name) {
            $this->downloadFile($name, $this->update);
        }
    } // }}} 
    // {{{ downloadFile
    /**
     * Download a file if it does not exist
     *
     * @param String $url Download URL
     * @param String $path Storage path
     * @param Boolean $force Force re-download of files
     *
     * @return boolean
     */
    protected function downloadFile($name, $update = false)
    {
        $subPath1 = '';
        if(substr($name, -strlen($this->postalCodePostfix))==$this->postalCodePostfix)
            $subPath1='/zip';

        $url = $this->files[$name]['url'];
        $storagePath = $this->config('geonames.storagePath');
        $txtFileName = $this->files[$name]['filename'] . '.txt';
        $urlSize = $this->getUrlSize($url);
        $urlFileName = basename($url);
        $this->line('Checking File: ' . $name . ' => ' . $urlFileName);
        $fileSize = @filesize($storagePath .$subPath1 . '/' . $urlFileName);
        if ($fileSize) {
            if ($fileSize === $urlSize) {
                $this->line($this->ansiFormat('File Exists:', Console::FG_YELLOW) . ' ' . $subPath1 . '/' . $urlFileName . ' with same size as remote geonames file exists.');
                return true;
            } else if ($fileSize !== $urlSize && !$update) {
                $this->line("File Size: $fileSize, URL Size: $urlSize");
                $this->line($this->ansiFormat('File Exists:', Console::FG_YELLOW) . ' ' . $urlFileName . ' with different size as remote geonames file exists. You should consider downloading newest files.');
                return true;
            } else {
                // If we are here, we will re-download zip file, so it is worthwhile to remoeve the old txt version first
                $extractedFilePath = $storagePath . '/' . $txtFileName;
                if (substr($urlFileName, -4) === '.zip' && file_exists($extractedFilePath)) {
                    $this->line($this->ansiFormat('Removing Old File:', Console::FG_YELLOW) . ' ' . basename($extractedFilePath));
                    unlink($extractedFilePath);
                }
            }
        }
        foreach(array('', '/zip') as $subPath) {
            // Create download directory
            if (!is_dir($storagePath.$subPath)) {
                mkdir($storagePath.$subPath, 0755, true);
            }
            // Create .gitignore if it does not exist in $storagePath
            if (!file_exists($storagePath.$subPath.'/.gitignore')) {
                $gitignore = "*\n!.gitignore\n";
                file_put_contents($storagePath.$subPath.'/.gitignore',$gitignore);
            }
        }
        // Open file and truncate it for writing (requires fopen_wrappers)
        $targetFile = $storagePath . $subPath1 . '/' . $urlFileName;
        $targetFP = fopen($targetFile, 'w+');
        if ($targetFP === false) {
            throw new RuntimeException('Can not open file for writing: ' . $targetFile);
        }
        $sourceFP = fopen($url, 'r', false, $this->getStreamContext());
        if ($sourceFP === false) {
            throw new RuntimeException('Can not open file for reading: ' . $url);
        }
        $bufferSize = 1024 * 1024;
        // Size should be at leaast 1 to show remaining time...
        $steps = $urlSize / $bufferSize + 1;
        /* @var $output OutputStyle */
        //$output = $this->getOutput();
        //$bar = $output->createProgressBar($steps);
        //$bar->setFormat('<info>Downloading:</info> ' . $urlFileName . ' %bar% %percent%% <info>Remaining Time:</info> %remaining%');
        //echo "Downloading $urlFileName...\n";
        Console::startProgress(0, $urlSize, 'Downloading: ' . $urlFileName.' ', false);
        $step=0;
        while (!feof($sourceFP)) {
            fwrite($targetFP, stream_get_contents($sourceFP, $bufferSize));
            // $bar->advance();
            $step++;
            Console::updateProgress($step*$bufferSize, $urlSize);
        }
        //$bar->finish();
        Console::updateProgress($urlSize, $urlSize);
        Console::endProgress("OK  " . PHP_EOL);
        //echo "\n"; //$output->newLine();
        clearstatcache(true, $targetFile);
        $this->line('File Downloaded: ' . $urlFileName . ' - ' . filesize($targetFile) . ' bytes.');
        return true;
    } // }}} 
    // {{{ unZip
    /**
     * Unzip the file
     *
     * @param  string $name Filename to be unzipped
     */
    protected function unZip($name)
    {
        $subPath = '';
        if(substr($name, -strlen($this->postalCodePostfix))==$this->postalCodePostfix)
            $subPath = '/zip';
        $zipFileName = basename($this->files[$name]['url']);
        if (!substr($zipFileName, -4) === '.zip')
            throw new RuntimeException($zipFileName . ' does not have .zip extension');
        // Final file path
        $storagePath = $this->config('geonames.storagePath').$subPath;
        $extractedFile = $this->files[$name]['filename'] . '.txt';
        $path = $storagePath . $subPath . '/' . $extractedFile;
        // Open zip archive because we need the size of extracted file
        $zipArchive = new \ZipArchive;
        $zipArchive->open($storagePath . '/' . $zipFileName);
        if (file_exists($path)) {
            $uncompressedSize = $zipArchive->statName($extractedFile)['size'];
            $fileSize = filesize($path);
            if ($uncompressedSize !== $fileSize) {
                $this->line($this->ansiFormat('Existing File:', Console::FG_YELLOW).' ' . basename($path) . ' size does not match the one in ' . $zipFileName);
            } else {
                // Do not extract again
                $this->line($this->ansiFormat('Existing File:', Console::FG_GREEN) . ' '. 'Found ' . basename($path) . ' file extracted from ' . $zipFileName);
                $zipArchive->close();
                return;
            }
        }
        // File does not exist or size does not match
        $this->line($this->ansiFormat('Extracting File:', Console::FG_GREEN) . ' ' . $extractedFile . ' from ' . $zipFileName . ' ...!!!Please Wait!!!...');
        // Extract file
        $zipArchive->extractTo($storagePath . '/', $extractedFile);
        $zipArchive->close();
    } // }}} 
    // {{{ getUrlSize
    /**
     * Get Remote File Size
     *
     * @param string $url remote address
     * @return int|boolean URL size in bytes or false
     */
    protected function getUrlSize($url)
    {

        $data = get_headers($url, true, $this->getStreamContext());
        if (isset($data['Content-Length']))
            return (int)$data['Content-Length'];
        return false;
    } // }}} 
    // {{{ getFilesArray
    /**
     * Returns files array after removing entries in
     * ignoreTables config option and adding custom countries from
     * countries config option if necessary
     *
     * @return array
     */
    protected function getFilesArray()
    {
        static $firstRun = true;
        if ($firstRun) {
            $this->updateFilesList();
            $firstRun = false;
        }
        $data = $this->files;
        foreach ($data as $key => $value) {
            if (in_array($value['table'], $this->config('geonames.ignoreTables'))) {
                unset($data[$key]);
            }
        }
        return $data;
    } // }}} 
    // {{{ updateFilesList
    protected function updateFilesList()
    {
        // Get ISO codes for countries to import if there are any
        $countries = $this->config('geonames.countries');
        // print_r($countries);

        if (!empty($countries)) {
            unset($this->files['allCountries']);
            unset($this->files['allCountriesPostCodes']);
            foreach ($countries as $country) {
                $this->files[$country] = [
                    'url' => 'http://download.geonames.org/export/dump/' . $country . '.zip',
                    'filename' => $country,
                    'table' => 'geoname'
                ];
                $this->files[$country.$this->postalCodePostfix] = [
                    'url' => 'http://download.geonames.org/export/zip/' . $country . '.zip',
                    'filename' => $country,
                    'table' => 'postalcode'
                ];
            }
        }
    } // }}} 
    // {{{ config
    /**
     * Read the module config file config/geonames.php
     */
    protected function config($key)
    {
        $cfgFile = Yii::getAlias('@app/config').'/geonames.php';
        if(!is_file($cfgFile))
            throw new yii\web\ServerErrorHttpException("Couldn't find config file: ".$cfgFile);
        $cfg = array();
        if(is_file($cfgFile)) {
            $cfg = require $cfgFile;
            $cfg = array_combine(
                array_map(function($k){ return 'geonames.'.$k; }, array_keys($cfg)),
                $cfg
            );
        }
        if(isset($cfg[$key]))
            return $cfg[$key];
        return null;
    } // }}} 
    // {{{ getStreamContext
    protected function getStreamContext()
    {
        $opts = array();
        if(!empty($this->config('geonames.proxy')) and !empty($this->config('geonames.proxy_user')) and !empty($this->config('geonames.proxy_pass'))) {
            $opts = array( 
                'http' => array(
                    'proxy' => 'tcp://'.$this->config('geonames.proxy'),
                    'request_fulluri'=>true,
                    'header' => array(
                        "Proxy-Authorization: Basic ".base64_encode($this->config('geonames.proxy_user').':'.$this->config('geonames.proxy_pass'))
                    )
                )
            );
        }
        return stream_context_create($opts);
    } // }}} 
    // {{{ line
    /**
     * Print out a messageand linefeed
     *
     * @param string $msg Message to be printed
     */
    protected function line($msg)
    {
        echo $msg."\n";
    } // }}} 
    // {{{ storeTimeStart
    protected function storeTimeStart()
    {
        list($usec, $sec) = explode(" ", microtime());
        $this->_timeStart = ((float)$usec + (float)$sec);
    } // }}} 
    // {{{ storeTimeEnd
    protected function storeTimeEnd()
    {
        list($usec, $sec) = explode(" ", microtime());
        $this->_timeEnd = ((float)$usec + (float)$sec);
    } // }}} 
    // {{{ getTimeDuration
    protected function getTimeDuration()
    {
        if(is_null($this->_timeEnd))
            $this->storeTimeEnd();
        return $this->_timeEnd - $this->_timeStart;
    } // }}} 
}
// {{{ storage_path
function Xstorage_path()
{
    return Yii::getAlias('@app/data');
} // }}} 

function geonames_microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}
