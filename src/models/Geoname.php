<?php

namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%geoname}}".
 *
 * @property int $geonameId
 * @property string $name
 * @property string $asciiName
 * @property string $alternateNames
 * @property string $latitude
 * @property string $longitude
 * @property string $featureClass
 * @property string $featureCode
 * @property string $countryCode
 * @property string $cc2
 * @property string $admin1Code
 * @property string $admin2Code
 * @property string $admin3Code
 * @property string $admin4Code
 * @property int $population
 * @property int $elevation
 * @property int $dem
 * @property string $timezoneId
 * @property string $modificationDate
 */
class Geoname extends \yii\db\ActiveRecord
{

    public $admin1Name;
    public $admin2Name;
    public $importance;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%geoname}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['geonameId'], 'required'],
            [['geonameId', 'population', 'elevation', 'dem'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['modificationDate'], 'safe'],
            [['name', 'asciiName', 'cc2'], 'string', 'max' => 200],
            [['alternateNames'], 'string', 'max' => 4000],
            [['featureClass'], 'string', 'max' => 1],
            [['featureCode'], 'string', 'max' => 10],
            [['countryCode'], 'string', 'max' => 2],
            [['admin1Code', 'admin3Code', 'admin4Code'], 'string', 'max' => 20],
            [['admin2Code'], 'string', 'max' => 80],
            [['timezoneId'], 'string', 'max' => 40],
            [['geonameId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'geonameId' => Yii::t('app', 'Geoname ID'),
            'name' => Yii::t('app', 'Name'),
            'asciiName' => Yii::t('app', 'ASCII Name'),
            'alternateNames' => Yii::t('app', 'Alternate Names'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'featureClass' => Yii::t('app', 'Feature Class'),
            'featureCode' => Yii::t('app', 'Feature Code'),
            'countryCode' => Yii::t('app', 'Country'),
            'cc2' => Yii::t('app', 'Cc2'),
            'admin1Code' => Yii::t('app', 'Admin1'),
            'admin2Code' => Yii::t('app', 'Admin2'),
            'admin3Code' => Yii::t('app', 'Admin3'),
            'admin4Code' => Yii::t('app', 'Admin4'),
            'population' => Yii::t('app', 'Population'),
            'elevation' => Yii::t('app', 'Elevation'),
            'dem' => Yii::t('app', 'DEM'),
            'timezoneId' => Yii::t('app', 'Timezone'),
            'modificationDate' => Yii::t('app', 'Modification Date'),
        ];
    }

    // {{{ getAlternateNames
    public function getAlternateNames()
    {
        return $this->hasMany(AlternateName::className(), ['geonameid' => 'geonameid']);
    }
    // }}}

    // {{{ getCountry
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryInfo::className(), ['isoAlpha2' => 'countryCode']);
    } // }}}

    // {{{ getTimezone
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['timeZoneId' => 'timezoneId']);
    } // }}}

    public function getAdmin1Model()
    {
        return $this->hasOne(Admin1CodeAscii::className(), ['code' => 'CONCAT_WS(\'.\', geoname.country, geoname.admin1Code)']);
    } // }}}

    /**
     * {@inheritdoc}
     * @return GeonameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GeonameQuery(get_called_class());
    }
}
