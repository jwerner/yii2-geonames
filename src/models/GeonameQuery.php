<?php

namespace diggindata\geonames\models;

/**
 * This is the ActiveQuery class for [[Geoname]].
 *
 * @see Geoname
 */
class GeonameQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Geoname[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Geoname|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function admin1Name()
    {
        return $this->leftJoin('{{%admin1codeascii}} admin1', 'CONCAT_WS(\'.\', geoname.countryCode, geoname.admin1Code)=admin1.code')
            ->addSelect(['{{%geoname}}.*', 'admin1.name as admin1Name']);
    }

    public function admin2Name()
    {
        return $this->leftJoin('{{%admin2code}} admin2', 'CONCAT_WS(\'.\', geoname.countryCode, geoname.admin1Code, geoname.admin2Code)=admin2.code')
            ->addSelect(['{{%geoname}}.*', 'admin2.name as admin2Name']);
    }

    public function city($name)
    {
        return $this->andFilterWhere(['like', 'geoname.name', $name.'%', false])
            ->andFilterWhere(['featureClass'=>['A', 'P']])->andFilterWhere(['featureCode'=>['PPLC', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4', 'ADM4' ]])
            // ->andFilterWhere(['featureClass'=>['P']])->andFilterWhere(['featureCode'=>['PPLC', 'PPLA', 'PPLA2', 'PPLA3', 'PPLA4' ]])
            ->addSelect(['{{%geoname}}.*', "CASE [[geoname.featureCode]] WHEN 'PPLC' THEN 1 WHEN 'PPLA' THEN 2 WHEN 'PPLA2' THEN 3 WHEN 'PPLA3' THEN 4 WHEN 'PPLA4' THEN 5 WHEN 'ADM4' THEN 5 ELSE 99 END AS importance"])
            ->orderBy(['importance' => SORT_ASC, 'geoname.name' => SORT_ASC]);
    }

    public function country($code)
    {
        return $this->andFilterWhere(['geoname.countryCode'=> $code])
            /*
            ->andFilterWhere(['featureClass'=>'A'])
            ->andFilterWhere(['featureCode'=>['PCLI']])
            ->orderBy(['population' => SORT_DESC])
             */
        ;
    }
}
