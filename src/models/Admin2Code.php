<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%admin2codes}}".
 *
 * @property string $code
 * @property string $name
 * @property string $nameAscii
 * @property int $geonameid
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class Admin2Code extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%admin2code}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'nameAscii'], 'string'],
            [['geonameId'], 'required'],
            [['geonameId'], 'integer'],
            [['code'], 'string', 'max' => 15],
            [['geonameId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'nameAscii' => Yii::t('app', 'Name Ascii'),
            'geonameId' => Yii::t('app', 'Geonameid'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return Admin2CodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Admin2CodeQuery(get_called_class());
    }
}
