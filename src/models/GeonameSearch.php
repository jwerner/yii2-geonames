<?php

namespace diggindata\geonames\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use diggindata\geonames\models\Geoname;

/**
 * GeonameSearch represents the model behind the search form of `app\models\Geoname`.
 */
class GeonameSearch extends Geoname
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['geonameId', 'population', 'elevation', 'dem'], 'integer'],
            [['name', 'asciiName', 'alternateNames', 'featureClass', 'featureCode', 'countryCode', 'cc2', 'admin1Code', 'admin2Code', 'admin3Code', 'admin4Code', 'timezoneId', 'modificationDate'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Geoname::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'geonameId' => $this->geonameId,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'population' => $this->population,
            'elevation' => $this->elevation,
            'dem' => $this->dem,
            'modificationDate' => $this->modificationDate,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name.'%', false])
            ->andFilterWhere(['like', 'asciiName', $this->asciiName])
            ->andFilterWhere(['like', 'alternateNames', $this->alternateNames])
            ->andFilterWhere(['like', 'featureClass', $this->featureClass])
            ->andFilterWhere(['like', 'featureCode', $this->featureCode])
            ->andFilterWhere(['like', 'countryCode', $this->countryCode])
            ->andFilterWhere(['like', 'cc2', $this->cc2])
            ->andFilterWhere(['like', 'admin1Code', $this->admin1Code])
            ->andFilterWhere(['like', 'admin2Code', $this->admin2Code])
            ->andFilterWhere(['like', 'admin3Code', $this->admin3Code])
            ->andFilterWhere(['like', 'admin4Code', $this->admin4Code])
            ->andFilterWhere(['like', 'timezoneId', $this->timezoneId]);

        return $dataProvider;
    }
}
