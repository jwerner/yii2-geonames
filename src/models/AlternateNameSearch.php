<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use diggindata\geonames\models\AlternateName;

/**
 * AlternateNameSearch represents the model behind the search form of `app\models\AlternateName`.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class AlternateNameSearch extends AlternateName
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alternatenameId', 'geonameId', 'isPreferredName', 'isShortName', 'isColloquial', 'isHistoric'], 'integer'],
            [['isoLanguage', 'alternateName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlternateName::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'alternatenameId' => $this->alternatenameId,
            'geonameId' => $this->geonameId,
            'isPreferredName' => $this->isPreferredName,
            'isShortName' => $this->isShortName,
            'isColloquial' => $this->isColloquial,
            'isHistoric' => $this->isHistoric,
        ]);

        $query->andFilterWhere(['like', 'isoLanguage', $this->isoLanguage])
            ->andFilterWhere(['like', 'alternateName', $this->alternateName]);

        return $dataProvider;
    }
}
