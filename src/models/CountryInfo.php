<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%countryinfo}}".
 *
 * @property string $iso_alpha2
 * @property string $iso_alpha3
 * @property int $iso_numeric
 * @property string $fips_code
 * @property string $name
 * @property string $capital
 * @property double $areainsqkm
 * @property int $population
 * @property string $continent
 * @property string $tld
 * @property string $currency
 * @property string $currencyName
 * @property string $Phone
 * @property string $postalCodeFormat
 * @property string $postalCodeRegex
 * @property int $geonameId
 * @property string $languages
 * @property string $neighbours
 * @property string $equivalentFipsCode
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class CountryInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%countryinfo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isoNumeric', 'population', 'geonameId'], 'integer'],
            [['areaInSqKm'], 'number'],
            [['isoAlpha2', 'continent'], 'string', 'max' => 2],
            [['isoAlpha3', 'fipsCode', 'tld', 'currency'], 'string', 'max' => 3],
            [['name', 'capital', 'languages'], 'string', 'max' => 200],
            [['currencyName'], 'string', 'max' => 20],
            [['phone', 'equivalentFipsCode'], 'string', 'max' => 10],
            [['postalCodeFormat', 'neighbours'], 'string', 'max' => 100],
            [['postalCodeRegex'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'isoAlpha2' => Yii::t('app', 'Iso Alpha2'),
            'isoAlpha3' => Yii::t('app', 'Iso Alpha3'),
            'isoNumeric' => Yii::t('app', 'Iso Numeric'),
            'fipsCode' => Yii::t('app', 'Fips Code'),
            'name' => Yii::t('app', 'Name'),
            'capital' => Yii::t('app', 'Capital'),
            'areaInSqKm' => Yii::t('app', 'Areainsqkm'),
            'population' => Yii::t('app', 'Population'),
            'continent' => Yii::t('app', 'Continent'),
            'tld' => Yii::t('app', 'Tld'),
            'currency' => Yii::t('app', 'Currency'),
            'currencyName' => Yii::t('app', 'Currency Name'),
            'phone' => Yii::t('app', 'Phone'),
            'postalCodeFormat' => Yii::t('app', 'Postal Code Format'),
            'postalCodeRegex' => Yii::t('app', 'Postal Code Regex'),
            'geonameId' => Yii::t('app', 'Geoname ID'),
            'languages' => Yii::t('app', 'Languages'),
            'neighbours' => Yii::t('app', 'Neighbours'),
            'equivalentFipsCode' => Yii::t('app', 'Equivalent Fips Code'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CountryInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CountryInfoQuery(get_called_class());
    }
}
