<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use diggindata\geonames\models\CountryInfo;

/**
 * CountryInfoSearch represents the model behind the search form of `app\models\CountryInfo`.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class CountryInfoSearch extends CountryInfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['isoAlpha2', 'isoAlpha3', 'fipsCode', 'name', 'capital', 'continent', 'tld', 'currency', 'currencyName', 'phone', 'postalCodeFormat', 'postalCodeRegex', 'languages', 'neighbours', 'equivalentFipsCode'], 'safe'],
            [['isoNumeric', 'population', 'geonameId'], 'integer'],
            [['areaInSqKm'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CountryInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'isoNumeric' => $this->isoNumeric,
            'areaInSqKm' => $this->areaInSqKm,
            'population' => $this->population,
            'geonameId' => $this->geonameId,
        ]);

        $query->andFilterWhere(['like', 'isoAlpha2', $this->isoAlpha2])
            ->andFilterWhere(['like', 'isoAlpha3', $this->isoAlpha3])
            ->andFilterWhere(['like', 'fipsCode', $this->fipsCode])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'capital', $this->capital])
            ->andFilterWhere(['like', 'continent', $this->continent])
            ->andFilterWhere(['like', 'tld', $this->tld])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'currencyName', $this->currencyName])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'postalCodeFormat', $this->postalCodeFormat])
            ->andFilterWhere(['like', 'postalCodeRegex', $this->postalCodeRegex])
            ->andFilterWhere(['like', 'languages', $this->languages])
            ->andFilterWhere(['like', 'neighbours', $this->neighbours])
            ->andFilterWhere(['like', 'equivalentFipsCode', $this->equivalentFipsCode]);

        return $dataProvider;
    }
}
