<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%alternatename}}".
 *
 * @property int $alternatenameId
 * @property int $geonameId
 * @property string $isoLanguage
 * @property string $alternateName
 * @property int $isPreferredName
 * @property int $isShortName
 * @property int $isColloquial
 * @property int $isHistoric
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class AlternateName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%alternatename}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alternatenameId'], 'required'],
            [['alternatenameId', 'geonameId', 'isPreferredName', 'isShortName', 'isColloquial', 'isHistoric'], 'integer'],
            [['isoLanguage'], 'string', 'max' => 7],
            [['alternateName'], 'string', 'max' => 200],
            [['alternatenameId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'alternatenameId' => Yii::t('app', 'Alternate Name ID'),
            'geonameId' => Yii::t('app', 'Geoname ID'),
            'isoLanguage' => Yii::t('app', 'ISO Language'),
            'alternateName' => Yii::t('app', 'Alternate Name'),
            'isPreferredName' => Yii::t('app', 'Is Preferred Name'),
            'isShortName' => Yii::t('app', 'Is Short Name'),
            'isColloquial' => Yii::t('app', 'Is Colloquial'),
            'isHistoric' => Yii::t('app', 'Is Historic'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AlternateNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlternateNameQuery(get_called_class());
    }
}
