<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%featurecodes}}".
 *
 * @property string $code
 * @property string $name
 * @property string $description
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class FeatureCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%featurecode}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['description'], 'string'],
            [['code'], 'string', 'max' => 7],
            [['name'], 'string', 'max' => 200],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return FeatureCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeatureCodeQuery(get_called_class());
    }
}
