<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use diggindata\geonames\models\PostalCode;

/**
 * PostalCodeSearch represents the model behind the search form of `app\models\PostalCode`.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class PostalCodeSearch extends PostalCode
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['countryCode', 'postalCode', 'placeName', 'admin1Name', 'admin1Code', 'admin2Name', 'admin2Code', 'admin3Name', 'admin3Code'], 'safe'],
            [['latitude', 'longitude'], 'number'],
            [['accuracy', 'id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostalCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'accuracy' => $this->accuracy,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'countryCode', $this->countryCode])
            ->andFilterWhere(['like', 'postalCode', $this->postalCode])
            ->andFilterWhere(['like', 'placeName', $this->placeName])
            ->andFilterWhere(['like', 'admin1Name', $this->admin1Name])
            ->andFilterWhere(['like', 'admin1Code', $this->admin1Code])
            ->andFilterWhere(['like', 'admin2Name', $this->admin2Name])
            ->andFilterWhere(['like', 'admin2Code', $this->admin2Code])
            ->andFilterWhere(['like', 'admin3Name', $this->admin3Name])
            ->andFilterWhere(['like', 'admin3Code', $this->admin3Code]);

        return $dataProvider;
    }
}
