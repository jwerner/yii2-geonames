<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%iso_languagecodes}}".
 *
 * @property string $iso_639_3
 * @property string $iso_639_2
 * @property string $iso_639_1
 * @property string $languageName
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class IsoLanguageCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%isolanguagecode}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iso_639_3'], 'string', 'max' => 4],
            [['iso_639_2', 'iso_639_1'], 'string', 'max' => 50],
            [['languageName'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iso_639_3' => Yii::t('app', 'ISO 639 3'),
            'iso_639_2' => Yii::t('app', 'ISO 639 2'),
            'iso_639_1' => Yii::t('app', 'ISO 639 1'),
            'languageName' => Yii::t('app', 'Language Name'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return IsoLanguageCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IsoLanguageCodeQuery(get_called_class());
    }
}
