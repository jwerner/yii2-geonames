<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%timezones}}".
 *
 * @property string $timeZoneId
 * @property string $countryCode
 * @property string $gmtOffset
 * @property string $dstOffset
 * @property string $rawOffset
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class Timezone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%timezone}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['timeZoneId'], 'required'],
            [['gmtOffset', 'dstOffset', 'rawOffset'], 'number'],
            [['timeZoneId'], 'string', 'max' => 40],
            [['countryCode'], 'string', 'max' => 2],
            [['timeZoneId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'timeZoneId' => Yii::t('app', 'Time Zone ID'),
            'countryCode' => Yii::t('app', 'Country Code'),
            'gmtOffset' => Yii::t('app', 'GMT Offset'),
            'dstOffset' => Yii::t('app', 'DST Offset'),
            'rawOffset' => Yii::t('app', 'RAW Offset'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TimezoneQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TimezoneQuery(get_called_class());
    }
}
