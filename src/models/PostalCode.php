<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use Yii;

/**
 * This is the model class for table "{{%postalcode}}".
 *
 * @property string $countryCode
 * @property string $postalCode
 * @property string $placeName
 * @property string $admin1Name
 * @property string $admin1Code
 * @property string $admin2Name
 * @property string $admin2Code
 * @property string $admin3Name
 * @property string $admin3Code
 * @property string $latitude
 * @property string $longitude
 * @property int $accuracy
 * @property int $id
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class PostalCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%postalcode}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude'], 'number'],
            [['accuracy'], 'integer'],
            [['countryCode'], 'string', 'max' => 2],
            [['postalCode', 'admin1Code', 'admin2Code', 'admin3Code'], 'string', 'max' => 20],
            [['placeName'], 'string', 'max' => 180],
            [['admin1Name', 'admin2Name', 'admin3Name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'countryCode' => 'Country Code',
            'postalCode' => 'Postal Code',
            'placeName' => 'Place Name',
            'admin1Name' => 'Admin1 Name',
            'admin1Code' => 'Admin1 Code',
            'admin2Name' => 'Admin2 Name',
            'admin2Code' => 'Admin2 Code',
            'admin3Name' => 'Admin3 Name',
            'admin3Code' => 'Admin3 Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'accuracy' => 'Accuracy',
            'id' => 'ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PostalCodeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostalCodeQuery(get_called_class());
    }

    public function getGeoname()
    {
        return $this->hasOne(Geoname::className(), ['admin2Code' => 'admin2Code', 'admin3Code' => 'admin3Code'])
            ->where(['geoname.featureClass' => 'P', 'geoname.featureCode'=>['PPLA', 'PPLA2', 'PPLA3']]);
    }


}
