<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

/**
 * This is the ActiveQuery class for [[AlternateName]].
 *
 * @see AlternateName
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class AlternateNameQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AlternateName[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AlternateName|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
