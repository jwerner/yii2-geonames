<?php
/*
 * This file is part of the Yii2 GeoNames extension.
 *
 * (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace diggindata\geonames\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */
class CitySearchForm extends Model
{
    public $query;
    public $countryCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name are required
            [['query'], 'required'],
            [['countryCode'], 'string', 'min'=>2, 'max'=>3],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'query' => 'Query',
            'countryCode' => 'Country Code',
        ];
    }

    public function search()
    {
        $result = array();
        echo '<hr>';
        if(!empty($this->countryCode)) {
            switch(strlen($this->countryCode))
            {
            case 2:;
                $countryInfos = CountryInfo::find()->where(['isoAlpha2'=>$this->countryCode])->one();
                break;
            case 3:
                $countryInfos = CountryInfo::find()->where(['isoAlpha3'=>$this->countryCode])->one();
                break;
            }
            if(!is_null($countryInfos)) {
                echo '<h3>Country Info</h3>';
                echo Yii::$app->controller->vd($countryInfos->attributes);
                // Do we have a postal code?
                if(preg_match('/'.$countryInfos->postalCodeRegex.'/', $this->query, $matches)) {
                    echo 'We have a postal code'.'<br>';
                    echo Yii::$app->controller->vd($matches);
                } else {
                    echo 'We don\'t have a postal code'.'<br>';
                    echo 'Searching for a name'.'<br>';
                    $records = Geoname::find()->admin1Name()->admin2Name()->city($this->query)->all(); 
                    foreach($records as $n=>$record) {
                        $result[] = $record->attributes;
                        $result[$n]['admin1Name'] = $record->admin1Name;
                        $result[$n]['admin2Name'] = $record->admin2Name;
                        $result[$n]['importance'] = $record->importance;
                    }
                    return $result;
                }
            }
        }
        echo '<hr>';
        return true; 
    }


}
