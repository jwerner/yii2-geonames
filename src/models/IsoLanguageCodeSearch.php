<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use diggindata\geonames\models\IsoLanguageCode;

/**
 * IsoLanguageCodeSearch represents the model behind the search form of `app\models\IsoLanguageCode`.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class IsoLanguageCodeSearch extends IsoLanguageCode
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iso_639_3', 'iso_639_2', 'iso_639_1', 'languageName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IsoLanguageCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'iso_639_3', $this->iso_639_3])
            ->andFilterWhere(['like', 'iso_639_2', $this->iso_639_2])
            ->andFilterWhere(['like', 'iso_639_1', $this->iso_639_1])
            ->andFilterWhere(['like', 'languageName', $this->languageName]);

        return $dataProvider;
    }
}
