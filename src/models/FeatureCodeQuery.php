<?php
/**
* This file is part of the Yii2 GeoNames extension.
*
* (c) yii2-geonames <https://bitbucket.org/jwerner/yii2-geonames/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
 */
 
namespace diggindata\geonames\models;

/**
 * This is the ActiveQuery class for [[FeatureCode]].
 *
 * @see FeatureCode
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */
class FeatureCodeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FeatureCode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FeatureCode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
