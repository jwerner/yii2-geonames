<?php

namespace diggindata\geonames;

use Yii;
use yii\base\BootstrapInterface;
use yii\console\Application as ConsoleApplication;
use yii\i18n\PhpMessageSource;

/**
 * Bootstrap class registers module. It also creates some url rules which will be applied
 * when UrlManager.enablePrettyUrl is enabled.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /** @var array Model's map */
    private $_modelMap = [
        'Admin1CodeAscii'   => 'diggindata\geonames\models\Admin1CodeAscii',
        'Admin2Code'        => 'diggindata\geonames\models\Admin2Code',
        'AlternateName'     => 'diggindata\geonames\models\AlternateName',
        'CountryInfo'       => 'diggindata\geonames\models\CountryInfo',
        'FeatureCode'       => 'diggindata\geonames\models\FeatureCode',
        'Geoname'           => 'diggindata\geonames\models\Geoname',
        'IsoLanguageCode'   => 'diggindata\geonames\models\IsoLanguageCode',
        'Timezone'          => 'diggindata\geonames\models\Timezone',
    ];

    /** @inheritdoc */
    public function bootstrap($app)
    {
        // echo "Module geonames bootstrap()\n";
        /** @var Module $module */
        /** @var \yii\db\ActiveRecord $modelName */
        if ($app->hasModule('geonames') && ($module = $app->getModule('geonames')) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $name => $definition) {
                $class = "diggindata\\geonames\\models\\" . $name;
                Yii::$container->set($class, $definition);
                $modelName = is_array($definition) ? $definition['class'] : $definition;
                $module->modelMap[$name] = $modelName;
            }

            if ($app instanceof ConsoleApplication) {
                // echo "bootstrap(): Set controllerNamespace\n";
                $module->controllerNamespace = 'diggindata\geonames\commands';
            } else {
                $configUrlRule = [
                    'prefix' => $module->urlPrefix,
                    // 'rules'  => $module->urlRules,
                ];

                if ($module->urlPrefix != 'geonames') {
                    $configUrlRule['routePrefix'] = 'geonames';
                }

                $configUrlRule['class'] = 'yii\web\GroupUrlRule';
                $rule = Yii::createObject($configUrlRule);

                $app->urlManager->addRules([$rule], false);

            }

            if (!isset($app->get('i18n')->translations['geonames*'])) {
                $app->get('i18n')->translations['geonames*'] = [
                    'class' => PhpMessageSource::className(),
                    'basePath' => __DIR__ . '/messages',
                    'sourceLanguage' => 'en-US'
                ];
            }

        }
    }

}
