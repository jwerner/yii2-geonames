<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CountryInfoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'iso_alpha2') ?>

    <?= $form->field($model, 'iso_alpha3') ?>

    <?= $form->field($model, 'iso_numeric') ?>

    <?= $form->field($model, 'fips_code') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'capital') ?>

    <?php // echo $form->field($model, 'areainsqkm') ?>

    <?php // echo $form->field($model, 'population') ?>

    <?php // echo $form->field($model, 'continent') ?>

    <?php // echo $form->field($model, 'tld') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'currencyName') ?>

    <?php // echo $form->field($model, 'Phone') ?>

    <?php // echo $form->field($model, 'postalCodeFormat') ?>

    <?php // echo $form->field($model, 'postalCodeRegex') ?>

    <?php // echo $form->field($model, 'geonameId') ?>

    <?php // echo $form->field($model, 'languages') ?>

    <?php // echo $form->field($model, 'neighbours') ?>

    <?php // echo $form->field($model, 'equivalentFipsCode') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
