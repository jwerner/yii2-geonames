<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CountryInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'isoAlpha2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isoAlpha3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isoNumeric')->textInput() ?>

    <?= $form->field($model, 'fipsCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capital')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'areaInSqKm')->textInput() ?>

    <?= $form->field($model, 'population')->textInput() ?>

    <?= $form->field($model, 'continent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tld')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'currencyName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postalCodeFormat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postalCodeRegex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'geonameId')->textInput() ?>

    <?= $form->field($model, 'languages')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'neighbours')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'equivalentFipsCode')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
