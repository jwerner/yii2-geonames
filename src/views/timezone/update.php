<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Timezone */

$this->title = Yii::t('app', 'Update Timezone: {name}', [
    'name' => $model->timeZoneId,
]);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Timezones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->timeZoneId, 'url' => ['view', 'id' => $model->timeZoneId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="timezone-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
