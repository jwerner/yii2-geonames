<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Timezone */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="timezone-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'timeZoneId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'countryCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gmtOffset')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dstOffset')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rawOffset')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
