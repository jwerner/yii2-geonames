<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TimezoneSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="timezone-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'timeZoneId') ?>

    <?= $form->field($model, 'country_code') ?>

    <?= $form->field($model, 'GMT_offset') ?>

    <?= $form->field($model, 'DST_offset') ?>

    <?= $form->field($model, 'RAW_offset') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
