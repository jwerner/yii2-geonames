<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IsoLanguageCode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="iso-language-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'iso_639_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iso_639_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iso_639_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'languageName')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
