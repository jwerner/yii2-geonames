<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IsoLanguageCode */

$this->title = Yii::t('app', 'Update Iso Language Code: {name}', [
    'name' => $model->iso_639_3,
]);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Iso Language Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iso_639_3, 'url' => ['view', 'id' => $model->iso_639_3]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="iso-language-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
