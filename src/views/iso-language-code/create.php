<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\IsoLanguageCode */

$this->title = Yii::t('app', 'Create Iso Language Code');
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Iso Language Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iso-language-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
