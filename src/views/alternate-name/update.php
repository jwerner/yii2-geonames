<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlternateName */

$this->title = Yii::t('app', 'Update Alternate Name: {name}', [
    'name' => $model->alternatenameId,
]);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alternate Names'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->alternatenameId, 'url' => ['view', 'id' => $model->alternatenameId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="alternate-name-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
