<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlternateName */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alternate-name-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'alternatenameId')->textInput() ?>

    <?= $form->field($model, 'geonameid')->textInput() ?>

    <?= $form->field($model, 'isoLanguage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alternateName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isPreferredName')->textInput() ?>

    <?= $form->field($model, 'isShortName')->textInput() ?>

    <?= $form->field($model, 'isColloquial')->textInput() ?>

    <?= $form->field($model, 'isHistoric')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
