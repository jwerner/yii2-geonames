<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlternateName */

$this->title = Yii::t('app', 'Create Alternate Name');
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alternate Names'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alternate-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
