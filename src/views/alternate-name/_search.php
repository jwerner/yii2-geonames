<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlternateNameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alternate-name-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'alternatenameId') ?>

    <?= $form->field($model, 'geonameid') ?>

    <?= $form->field($model, 'isoLanguage') ?>

    <?= $form->field($model, 'alternateName') ?>

    <?= $form->field($model, 'isPreferredName') ?>

    <?php // echo $form->field($model, 'isShortName') ?>

    <?php // echo $form->field($model, 'isColloquial') ?>

    <?php // echo $form->field($model, 'isHistoric') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
