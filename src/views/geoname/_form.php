<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Geoname */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geoname-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'geonameId')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'asciiName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alternateNames')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'featureClass')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'featureCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'countryCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cc2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin1Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin2Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin3Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin4Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'population')->textInput() ?>

    <?= $form->field($model, 'elevation')->textInput() ?>

    <?= $form->field($model, 'dem')->textInput() ?>

    <?= $form->field($model, 'timezoneId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modificationDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
