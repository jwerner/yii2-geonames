<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Geoname */

$this->title = Yii::t('app', 'Create Geoname');
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geonames'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="geoname-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
