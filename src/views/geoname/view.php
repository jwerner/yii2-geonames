<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Geoname */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Geonames'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="geoname-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->geonameId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->geonameId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'geonameId',
            'name',
            'asciiName',
            'alternateNames',
            'latitude',
            'longitude',
            'featureClass',
            'featureCode',
            'countryCode',
            'cc2',
            'admin1Code',
            'admin1Name',
            'admin2Code',
            'admin2Name',
            'admin3Code',
            'admin4Code',
            'population',
            'elevation',
            'dem',
            'timezoneId',
            'modificationDate',
        ],
    ]) ?>

    <?= '' // \yii\helpers\VarDumper::dump($model->admin1_name, 10, true) ?>

    <h2><?= 'Country' ?></h2>
    <?= DetailView::widget([
        'model' => $model->country,
        'attributes' => [
            'isoAlpha2',
            'name',
            'capital',
            'continent',
            'currency',
        ],
    ]) ?>

    <h2><?= 'Timezone' ?></h2>
    <?= DetailView::widget([
        'model' => $model->timezone,
        'attributes' => [
            'timeZoneId',
            'countryCode',
            'gmtOffset',
            'dstOffset',
            'rawOffset',
        ],
    ]) ?>
    
</div>
