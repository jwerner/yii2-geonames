<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GeonameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geoname-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'geonameid') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'asciiname') ?>

    <?= $form->field($model, 'alternatenames') ?>

    <?= $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'fclass') ?>

    <?php // echo $form->field($model, 'fcode') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'cc2') ?>

    <?php // echo $form->field($model, 'admin1') ?>

    <?php // echo $form->field($model, 'admin2') ?>

    <?php // echo $form->field($model, 'admin3') ?>

    <?php // echo $form->field($model, 'admin4') ?>

    <?php // echo $form->field($model, 'population') ?>

    <?php // echo $form->field($model, 'elevation') ?>

    <?php // echo $form->field($model, 'gtopo30') ?>

    <?php // echo $form->field($model, 'timezone') ?>

    <?php // echo $form->field($model, 'moddate') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
