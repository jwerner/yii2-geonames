<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admin1CodesAscii */

$this->title = Yii::t('app', 'Update Admin1 Codes Ascii: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin1 Codes ASCII'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin1-codes-ascii-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
