<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admin1CodesAscii */

$this->title = Yii::t('app', 'Create Admin1 Codes Ascii');
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin1 Codes Asciis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin1-codes-ascii-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
