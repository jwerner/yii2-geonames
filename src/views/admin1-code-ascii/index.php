<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Admin1CodesAsciiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Admin1 Codes Asciis');
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin1-codes-ascii-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Admin1 Codes Ascii'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}&nbsp;{update}'
            ],

            'code',
            'name:ntext',
            'nameAscii:ntext',
            'geonameId',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}'
            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
