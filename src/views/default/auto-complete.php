<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CitySearchForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

$this->title = 'Auto-Complete';
$this->params['breadcrumbs'][] = $this->title;

// Get API Key
$your_api_key = 'INSERT_YOUR_GOOGLE_MAPS_API_KEY_HERE';

?>

<style>
/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
.map-wrapper {
  padding: 10px;
  border: 1px solid navy;
}
#map {
  height: 400px;
}
</style>
<div class="auto-complete">
    <h1><?= Html::encode($this->title) ?></h1>

    <form id="search-form" class="form-horizontal">

        <div class="form-group required">
            <label class="col-lg-1 control-label" for="citysearch">Search:</label>
            <div class="col-lg-3">
                <?= AutoComplete::widget([
                    'id'=>'citysearch',
                    'name'=>'citysearch',
                    'clientOptions' => [
                        'source' => Url::to(['city-autocomplete']),
                        'minLength'=>'2',
                        'select' => new JsExpression('function( event, ui ) {
                            console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
                            $.getJSON("' . Url::to(['//geonames/geoname/get-json']) . '&id=" + ui.item.id, function(result){
                                console.log(result);
                                $("#result").html(JSON.stringify(result));
                                locationFound = result.result;
                                map.setCenter(locationFound.geometry.location);
                                // searchLocationsNear(locationFound.geometry.location);
                            });
                        }'),
                    ],
                    'options'=>[
                        'class' => 'form-control'
                    ]
                ]); ?>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-lg-1 control-label" for="radiusSelect">Radius:</label>
            <div class="col-lg-3">
                <select id="radiusSelect" class="form-control">
                    <option value="50" selected>50 kms</option>
                    <option value="30">30 kms</option>
                    <option value="20">20 kms</option>
                    <option value="10">10 kms</option>
                    <option value="5">5 kms</option>
                    <option value="1">1 km</option>
                </select>
            </div>
        </div>

        <div class="form-group required" id="locationSelectContainer" style="visibility: hidden">
            <label class="col-lg-1 control-label" for="locationSelect">Location:</label>
            <div class="col-lg-3">
                <select id="locationSelect" class="form-control"></select>
            </div>
        </div>
    </form>
    <div id="result"></div>
    <div><strong>Note:</strong> Please insert your Google Maps API key in this page's code at <code><?= __FILE__ ?></code>.</div>

    <div class="map-wrapper"><div id="map"></div>
</div>
<script>
var map;
var markers = [];
var infoWindow;
var locationSelect;
var locationSelectContainer;
var locationFound;
var opel = {lat: 49.986879, lng: 8.4080063};

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: opel,
        zoom: 11,
        mapTypeId: 'roadmap',
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });
    infoWindow = new google.maps.InfoWindow();

    //searchButton = document.getElementById("searchButton").onclick = searchLocations;

    locationSelect = document.getElementById("locationSelect");
    locationSelectContainer = document.getElementById("locationSelectContainer");
    locationSelect.onchange = function() {
        var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
        if (markerNum != "none"){
            google.maps.event.trigger(markers[markerNum], 'click');
        }
    };
}

function clearLocations() {
    infoWindow.close();
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    markers.length = 0;

    locationSelect.innerHTML = "";
    var option = document.createElement("option");
    option.value = "none";
    option.innerHTML = "See all results:";
    locationSelect.appendChild(option);
}

function searchLocationsNear(center) {
    clearLocations();
    console.log(center);
    var radius = document.getElementById('radiusSelect').value;
    var searchUrl = 'index.php?r=maps%2Fsearch&SearchForm[lat]=' + center.lat + '&SearchForm[lng]=' + center.lng + '&SearchForm[radius]=' + radius;
    downloadUrl(searchUrl, function(data) {
        var xml = parseXml(data);
        var markerNodes = xml.documentElement.getElementsByTagName("marker");
        var bounds = new google.maps.LatLngBounds();
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for (var i = 0; i < markerNodes.length; i++) {
            var id = markerNodes[i].getAttribute("id");
            var name = markerNodes[i].getAttribute("name");
            var address = markerNodes[i].getAttribute("address");
            var distance = parseFloat(markerNodes[i].getAttribute("distance"));
            var latlng = new google.maps.LatLng(
                parseFloat(markerNodes[i].getAttribute("lat")),
                parseFloat(markerNodes[i].getAttribute("lng")));

            createOption(name, distance, i);
            createMarker(latlng, name, address, labels[i]);
            bounds.extend(latlng);
        }
        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'img/markerclusterer/m'});
        
        // Only apply bounds if we received some markers
        if(markerNodes.length>0)
            map.fitBounds(bounds);
        else
            map.setCenter(locationFound.geometry.location);

        locationSelectContainer.style.visibility = "visible";
        //locationSelect.style.visibility = "visible";
        locationSelect.onchange = function() {
            var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
            google.maps.event.trigger(markers[markerNum], 'click');
        };
    });
}

function createMarker(latlng, name, address, label) {
    var html = "<b>" + name + "</b> <br/>" + address;
    var marker = new google.maps.Marker({
        // map: map,
        label: label, 
        position: latlng,
        icon: 'img/Location-Pin-Text_S_BL-Y-NEG_All_150928_28x40.png'
    });
    google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
    });
    markers.push(marker);
}

function createOption(name, distance, num) {
    var option = document.createElement("option");
    option.value = num;
    option.innerHTML = name;
    locationSelect.appendChild(option);
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request.responseText, request.status);
        }
    };

    request.open('GET', url, true);
    request.send(null);
}


function doNothing() {}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=<?= $your_api_key ?>&callback=initMap">
</script>
