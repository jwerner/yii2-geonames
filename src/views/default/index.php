<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'GeoNames Module';
?>
<div class="geonames-default-index">

    <div class="jumbotron">
        <h1>GeoNames with Yii!</h1>

        <p class="lead">A GeoNames sample database</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Search a City</h2>

                <p>Search for a city name</p>

                <p><?= Html::a('View Example &raquo;', ['search-city'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>City Auto-Complete</h2>

                <p>Search for a city name</p>

                <p><?= Html::a('View Example &raquo;', ['auto-complete'], ['class'=>'btn btn-default']) ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <h2>GeoNames</h2>

                <p>Contains all cities with a population > 5000 or PPLA (ca 50.000), see 'geoname' table for columns</p>

                <p><?= Html::a('GeoNames &raquo;', ['geoname/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Alternate Names</h2>

                <p>Contains alternative names for different languages for a geonameid</p>

                <p><?= Html::a('Alternate Names &raquo;', ['alternate-name/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Country Infos</h2>

                <p>Contains country information : iso codes, fips codes, languages, capital ,...<br>
                    see the geonames webservices for additional country information,<br>
                    bounding box                         : http://api.geonames.org/countryInfo?<br>
                    country names in different languages : http:/api.geonames.org/countryInfoCSV?lang=it</p>

                <p><?= Html::a('Country Infos &raquo;', ['country-info/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Admin1 Codes ASCII</h2>

                <p>Contains names in English for admin divisions.<br>
                    Columns: code, name, name ascii, geonameid</p>

                <p><?= Html::a('Admin1 Codes ASCII &raquo;', ['admin1-code-ascii/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Admin2 Codes</h2>

                <p>Contains names for administrative subdivision <i>admin2 code</i> (UTF8),<br>
                    Format : concatenated codes <tab>name <tab> asciiname <tab> geonameId</p>

                <p><?= Html::a('Admin2 Codes &raquo;', ['admin2-code/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Feature Codes</h2>

                <p>Contains name and description for feature classes and feature codes </p>

                <p><?= Html::a('Feature Codes &raquo;', ['feature-code/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <h2>Timezones</h2>

                <p>Contains countryCode, timezoneId, gmt offset on 1st of January, dst offset to gmt on 1st of July (of the current year), rawOffset without DST</p>

                <p><?= Html::a('Timezones &raquo;', ['timezone/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>ISO Language Codes</h2>

                <p>Contains country information : iso codes, fips codes, languages, capital ,...<br>
                    see the geonames webservices for additional country information,<br>
                    bounding box                         : http://api.geonames.org/countryInfo?<br>
                    country names in different languages : http:/api.geonames.org/countryInfoCSV?lang=it</p>

                <p><?= Html::a('ISO Language Codes &raquo;', ['iso-language-code/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Postal Codes</h2>

                <p>Cities and Postal Codes</p>

                <p><?= Html::a('Postal Codes &raquo;', ['postal-code/index'], ['class'=>'btn btn-default']) ?></p>
            </div>
        </div>
    </div>
</div>
