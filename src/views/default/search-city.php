<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CitySearchForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'City Search';
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Search for a city name or postal code
    </p>

    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'city-search-form']); ?>

                <?= $form->field($model, 'query')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'countryCode') ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

    <?php if (Yii::$app->session->hasFlash('citySearchFormSubmitted')): ?>
    <h2><?= 'DEBUG ' ?></h2>
    <?= $debug ?>
    <?php endif; ?>

</div>
