<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostalCode */

$this->title = 'Create Postal Code';
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => 'Postal Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postal-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
