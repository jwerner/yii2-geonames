<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model diggindata\geonames\PostalCodeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postal-code-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'countryCode') ?>

    <?= $form->field($model, 'postalCode') ?>

    <?= $form->field($model, 'placeName') ?>

    <?= $form->field($model, 'admin1Name') ?>

    <?= $form->field($model, 'admin1Code') ?>

    <?php // echo $form->field($model, 'admin2Name') ?>

    <?php // echo $form->field($model, 'admin2Code') ?>

    <?php // echo $form->field($model, 'admin3Name') ?>

    <?php // echo $form->field($model, 'admin3Code') ?>

    <?php // echo $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'accuracy') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
