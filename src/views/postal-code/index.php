<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel diggindata\geonames\PostalCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Postal Codes';
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postal-code-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Postal Code', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}&nbsp;{update}'
            ],

            'countryCode',
            'postalCode',
            'placeName',
            'admin1Name',
            'admin1Code',
            'admin2Name',
            'admin2Code',
            'admin3Name',
            'admin3Code',
            //'latitude',
            //'longitude',
            //'accuracy',
            'id',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
