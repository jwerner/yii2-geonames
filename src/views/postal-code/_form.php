<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostalCode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postal-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'countryCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postalCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'placeName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin1Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin1Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin2Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin2Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin3Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'admin3Code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accuracy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
