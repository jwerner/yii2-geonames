<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostalCode */

$this->title = 'Update Postal Code: ' . sprintf('%s - %s (%s)', $model->countryCode, $model->placeName, $model->postalCode);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => 'Postal Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="postal-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
