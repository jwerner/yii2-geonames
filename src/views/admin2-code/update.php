<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admin2Code */

$this->title = Yii::t('app', 'Update Admin2 Code: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['url'=>['/geonames'], 'label'=>'GeoNames'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin2 Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->geonameId]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin2-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
