GeoNames Extension for Yii 2
============================

This extension provides a **GeoNames** management solution for [Yii framework 2.0](http://www.yiiframework.com), containing import scripts and a CRUD UI.

It is an adaption of the **Laravel / Lumen / Eloquent Geonames** scripts at [https://github.com/yurtesen/geonames](https://github.com/yurtesen/geonames)

For license information check the [LICENSE](LICENSE.md)-file.

[Latest Stable Version](https://packagist.org/packages/diggindata/yii2-geonames)


## 1. Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist diggindata/yii2-geonames:dev-master
```

or

```
php composer.phar require --prefer-dist diggindata/yii2-geonames
```

or add

```json
"diggindata/yii2-geonames": "@dev"
```

to the require section of your composer.json, then run

```
composer install
```

## 2. Configuration

### Folders

We need a folder to store downloaded data files from geonames.org.
So in your application's base directory, create the directories `data` and `data/geonames`.
This directory will hold the downloaded data files from __geonames.org__.

### Application Configuration

Add following lines to both, your web and your console configuration file to enable this module (`config/web.php` and `config/console.php`):

```php
    ...
    'modules' => [
        ...
        'geonames' => [
            'class' => 'diggindata\geonames\Module',
        ],
    ],
```

### Commandline Configuration

The `yii geonames` shell command can be configured.

Create a file `geonames.php` in the `config` directory of your Yii application.

You may use the file `vendoe/diggindata/yii2-geonames/geonames.php.example` file as a template.

All available configuration options are listed below with their default values.

---

### proxy (type: `string`, default: `null`)

* URL with port of proxy server

### proxy_user (type: `string`, default: `null`)

* Proxy usernanme

### proxy_pass (type: `string`, default: `null`)

* Proxy user password

### keepTxt (type: `boolean`, default: `true`)

### storagePath (default: `Yii::getAlias('@app/data') . '/geonames'`)

### ignoreTables

Array of tables which would be ignored in imports. Some tables might note be used by you, e.g. alternate_names table.  
Uncommenting it will stop auto-import.

### countries (type: `array`, default: `array()`)

Array of 2-Char ISO codes of countries which shall be imported. If the array is empty, the `allCountries` file is imported.


## 3. Update Database Schema

Finally you need to update your database schema by applying the provided migrations. Make sure that you have properly configured your `db` application component, then run the following command:

```bash
$ php yii migrate/up --migrationPath=@vendor/diggindata/yii2-geonames/src/migrations
```

# Where do I go now?

By now you should have Yii2-geonames installed. You may now open the `geonames` module.
You should also be able to import geonames data via the console commqand, see `Usage`below.


# Usage

## Download Data

Enter the following command within your application's folder:

```
yii geonames/geonames/download
```

This will download data files:

```
[] zip
+--- DE.zip
iso-languagecodes.txt
timeZones.txt
admin1CodesASCII.txt
admin2Codes.txt
countryInfo.txt
DE.zip
featureCodes_en.txt
hierarchy.zip
```

To update already existing files, add the `-u` flag.

## Import Data

Enter the following command within your application's folder:

```
yii geonames/geonames/seed
```

This will insert the downloaded data into the respective database tables.


# Tipps

## Delete duplicate postal codes

```
-- Add Auto-Increment PK
ALTER TABLE `postalcode`
ADD `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY;

-- Delete duplicates
DELETE t1 FROM postalcode t1
INNER JOIN postalcode t2 
WHERE 
    t1.id > t2.id AND (
    t1.countryCode = t2.countryCode AND
    t1.postalCode = t2.postalCode AND
    t1.placeName = t2.placeName AND
    t1.admin1Code = t2.Admin1Code AND
    t1.admin2Code = t2.admin2Code AND
    t1.admin3Code = t2.admin3Code AND
    t1.latitude = t2.latitude AND
    t1.longitude = t2.longitude
);

-- Drop Auto-Increment PK
ALTER TABLE postalcode DROP `id`;
```


